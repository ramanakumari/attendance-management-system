package com.bean.ams.employeeImp;

import java.io.Serializable;

import com.bean.ams.util.EmployeePozo;
/**
 * This UserLogin class is used to set and get the values 
 * @author Batch-'G'
 */
public class UserLogin implements Serializable{
	EmployeePozo employee = new EmployeePozo();
	private static final long serialVersionUID = 1L;
		@SuppressWarnings("unused")
		private String name;
		private String password;
		@SuppressWarnings("unused")
		private String confirmPassword;
		@SuppressWarnings("unused")
		private String confirmName;
		//constructor with zero arguments
		public UserLogin()
		{
			
		}
		//getter method to get name
		
		//setter method to set name
		public void setName(String name) {
			this.name = name;
		}
		//getter method to get password
		public String getPassword() {
			return password;
		}
		//setter method to set name
		public void setConfirmName(String confirmName) 
		{
			if(employee.validateName(employee.getPassword(),confirmName))
			{
				this.confirmName = confirmName;
			}
			else
			{
				System.out.println("password and confirm password doesn't matches....plz re-enter.....");
			}
		}
		public void setConfirmPassword(String confirmPassword) 
		{
			if(employee.validatePassword(employee.getPassword(),confirmPassword))
			{
				this.confirmPassword = confirmPassword;
			}
			else
			{
				System.out.println("password and confirm password doesn't matches....plz re-enter.....");
			}
		}
	}

