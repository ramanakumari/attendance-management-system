package com.bean.ams.employeeImp;

import java.util.Scanner;

import com.bean.ams.readwritefile.File_Read;
import com.bean.ams.util.EmployeePozo;
/**
 * This c class is used to Register the new employee by using valid employee details.
 * @author Batch-'G'
 *
 */
public class EmployeeRegistraionImpl {
		int flag=0;
		Scanner sc = new Scanner(System.in);
		/**
		 * This employeeRegstrationImpl method is used to  Register the new employee by using valid employee details.
		 */
		public void employeeRegstrationImpl() {   
		EmployeePozo userRegistration = new EmployeePozo();
		while(flag == 0){
			System.out.println("enter name");
			String name = sc.next();
			userRegistration.setName(name);
			if(userRegistration.getName()!=null){
				flag = 1;
			}
		}
		flag = 0;
		while(flag == 0){
			System.out.println("enter password");
			String password = sc.next();
			userRegistration.setPassword(password);
			if(userRegistration.getPassword() != null){
				flag = 1;
			}
		}
		flag = 0;
		while(flag == 0){
			System.out.println("enter confirm password");
			String confirmPassword = sc.next();
			userRegistration.setConfirmPassword(confirmPassword);
			if(userRegistration.getConfirmPassword() != null){
				flag = 1;
			}
		}
		flag = 0;
		while(flag == 0){
			System.out.println("enter mobile number");
			String mobileNumber = sc.next();
			userRegistration.setMobileNumber(mobileNumber);
			if(userRegistration.getMobileNumber() != null){
				flag = 1;
			}
		}
		System.out.println("enter address");
		String address = sc.next();
		userRegistration.setAddress(address);
		flag = 0;
		while(flag == 0){
			System.out.println("enter emailId");
			String emailId = sc.next();
			userRegistration.setEmailId(emailId);
			if(userRegistration.getEmailId() != null){
				flag = 1;
			}
		}
		flag=0;
		while(flag == 0) {
			System.out.println("enter which animal do you like");
			String animal = sc.next();
				userRegistration.setAnimal(animal);
				
				if(userRegistration.getAnimal()  != null){
					
					flag = 1;
				}
		}
		
		System.out.println("Registered successful....");
		
		File_Read.writeFile(userRegistration);
		
	}
}
