package com.bean.ams.employeeImp;

import java.io.Serializable;
/**
 * This EmployeeAttendence class is used to construct set and get methods for the employee.
 * @author Batch-'G'
 *
 */
@SuppressWarnings("serial")
public class EmployeeAttendence implements Serializable {
	private String name;
	private String password;
	private String Date;
	/**
	* The getName() method returns the name 
	* @return name
	*/
	public String getName() {
		return name;
	}
	/**
	* The setName() method set the name of the employee
	*/
	public void setName(String name) {
		this.name = name;
	}
	/**
	* The getPassword() method returns the name 
	* @return name
	*/
	public String getPassword() {
		return password;
	}
	/**
	* The setPassword() method set the password
	*/
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	* The getDate() method returns the name 
	* @return Date
	*/
	public String getDate() {
		return Date;
	}
	/**
	* The setName() method set the date
	*/
	public void setDate(String date) {
		Date = date;
	}
}
