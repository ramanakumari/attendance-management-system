package com.bean.ams.mainlogin;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MonthlyDetails implements Serializable{
	private int month;
	private int noOfDays;
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}
	
}
