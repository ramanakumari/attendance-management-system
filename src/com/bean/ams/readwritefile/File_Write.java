package com.bean.ams.readwritefile;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import javax.imageio.IIOException;

import com.bean.ams.adminlogin.AdminLogin;
import com.bean.ams.adminlogin.AdminLoginImpl;
import com.bean.ams.employeeImp.EmployeeAttendence;
import com.bean.ams.mainlogin.MonthlyDetails;
import com.bean.ams.util.EmployeePozo;

/**
 * This class File_Write is used to store the data into a file.
 * 
 * @author Batch-'G'
 *
 */
@SuppressWarnings("serial")
public class File_Write implements Serializable{
	static EmployeePozo employee = new EmployeePozo();

	/**
	 * This fileWrite1 method is used to read the existed data from the file and
	 * again store the user input data including existing data
	 * @return 
	 * 
	 * @throws IOException
	 */

	public static boolean fileWrite1() throws IOException ,NullPointerException{
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		boolean flag = false;
		int flag1 = 0;
		@SuppressWarnings("unused")
		int count = 0;
		String username = null;
		String password = null;
		int attempts = 1;
		while (attempts <= 3) {

	public class File_Write {
		static EmployeePozo employee =new EmployeePozo();
		/**
		 *This fileWrite1 method  is used to read the existed data from the file and again store the user input data including existing data
		 * @throws IOException 
		 */
		public void fileWrite1() throws IOException {
			@SuppressWarnings("resource")
			Scanner sc = new Scanner(System.in);
			int flag1=0;
			String username = null;
			String password = null;
			int attempts = 1;
			//while (attempts <= 3) {
			while (flag1 == 0 && attempts <= 3) {
		
			

			System.out.println("enter username");
			username = sc.next();
			System.out.println("enter password");
			password = sc.next();

			//EmployeePozo pozo = new EmployeePozo();

			List<EmployeePozo> user = new ArrayList<EmployeePozo>();

			user = File_Read.fileRead();
			Calendar cal = null;

			List<EmployeePozo> user=new ArrayList<EmployeePozo>();
			user=File_Read.fileRead();
			Calendar cal=null;

			cal = Calendar.getInstance();

			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			@SuppressWarnings("unused")
			String formatted = format1.format(cal.getTime());

			for (EmployeePozo pozo : user) {
				if (pozo.getName().equals(username) && pozo.getPassword().equals(password)) {
					// System.out.println("successfuly loggined");
					// System.out.println(pozo);
					flag1 = 1;
					break;
				}
			}
			if (flag1 == 1) {
				System.out.println("successfuly loggined");
				List<EmployeeAttendence> user1 = null;
				user1 = new ArrayList<EmployeeAttendence>();
				user1 = filereadAttendence();
				if (user1 != null) {
					//System.out.println("hai");
					for (EmployeeAttendence attendance : user1) {
						//System.out.println(attendance.getDate().equals("2018-12-28"));
						if (attendance.getPassword().equals(password) && ((attendance.getDate()).equals("2018-12-28"))) {
							System.out.println("hello");
							flag = true;
							
							System.out.println("you have  already loggined");
							return;
						}
					}

				//System.out.println("attendance");
				//monthlyAttendance_write("2018-12-26");
				//display();
			break;
				// System.out.println(pozo);
			}
			}
		//		if (flag1 == 1) {
			//	}
				if (flag == false && flag1 == 1) {
					EmployeeAttendence obj = new EmployeeAttendence();
					obj.setDate("2018-12-28");
					obj.setName(username);
					obj.setPassword(password);
					filewriteAttendence(obj);
				}
				else {
				System.out.println("not loggined");
				attempts++;
			}
			if (attempts == 4) {
				System.out.println("you ran out of attempts. Thank you..!");
				System.out.println("enter which animal do you like");
				String animal = sc.next();	
				try {
					for (EmployeePozo pozo1 : user) {
						if (pozo1.getAnimal().equals(animal)) {
					System.out.println("Username:" + pozo1.getName());
					System.out.println("Password:" + pozo1.getPassword());
					break;
						}	
				}
				}catch(Exception e) {
					
				}
			for(EmployeePozo pozo:user) {
				  if(pozo.getName().equals(username) && pozo.getPassword().equals(password)) {
					  System.out.println("successfuly loggined");
					  System.out.println(pozo);
					  flag1=1;
					  return;
				  }
				  }
			
                  if(flag1==1) {
                	  boolean flag=false;
					  ArrayList<EmployeeAttendence> user1=null;
					  user1=new   ArrayList<EmployeeAttendence>();
					  user1=filereadAttendence();
					  if( user1!=null) {
						  for(EmployeeAttendence attendance:user1) {
							  if( attendance.getPassword().equals(password) && ((attendance.getDate()).equals("2018-12-25"))) {
								  flag=true;
								  System.out.println("you have  already loggined");
								  break;
							  }
						  }
					  }
                     if(flag==false && flag1==1) {
                    	 EmployeeAttendence obj=new EmployeeAttendence();
                    	 obj.setDate("2018-12-25");
                    	 obj.setName(username);
                    	 obj.setPassword(password);
                    	 filewriteAttendence( obj);
                     }
                  }
			   else {	  
				     System.out.println(" not loggined");
				     attempts++;
               }
		       
			}
			
			return;
		   }
		

	/**
	 * This adminFilewrite method is used to write the admin login credentials into the
	 * file.
	 * 
	 * @param admin
	 */
		public void adminFileWrite(AdminLogin admin) {
		try {
			FileOutputStream fos = new FileOutputStream("Data1.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(admin);
			System.out.println("hello");
			oos.close();
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	/**
	 * This fileDelete method is used to delete the particular employee details from
	 * the file.
	 * 
	 * @param mobileno
	 * @return
	 */
	public static boolean fileDelete(String mobileno) {
		ArrayList<EmployeePozo> user = new ArrayList<EmployeePozo>();
		user = (ArrayList<EmployeePozo>) File_Read.fileRead();
		@SuppressWarnings("unused")

		int count = -1;
		for (EmployeePozo p : user) {
			count++;
			if (p.getMobileNumber().equals(mobileno)) {
				user.remove(p);
				break;

		public static boolean fileUpdate(String mobileno,String mobilno1) {
			 ArrayList<EmployeePozo> user=new ArrayList<EmployeePozo>();
			 user=(ArrayList<EmployeePozo>)File_Read.fileRead();
			 int count=-1;
			 for(EmployeePozo p:user) {
				 count++;
				 if(p.getMobileNumber().equals(mobileno)) {
					 p.setMobileNumber(mobilno1);
					 System.out.println(p);
					 return true;
					 
					 
				 } 
	       }
	      

		try {
			FileOutputStream fos = new FileOutputStream("Data2.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(user);
			oos.close();
			fos.close();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return false;
	}

	/**
	 * This fileUpdate method is used to update the particular employee details into
	 * the file.
	 * 
	 * @param mobileno
	 * @param mobilno1
	 */
	@SuppressWarnings("unused")
	public static void fileUpdate(String mobileno, String mobilno1) {
		ArrayList<EmployeePozo> user = new ArrayList<EmployeePozo>();
		user = (ArrayList<EmployeePozo>) File_Read.fileRead();
		int count = -1;
		for (EmployeePozo p : user) {
			count++;
			if (p.getMobileNumber().equals(mobileno)) {
				p.setMobileNumber(mobilno1);
				System.out.println(p);
				break;

		@SuppressWarnings({ "unchecked", "resource", "rawtypes" })
		/**
		 * This class  filereadAttendence is used to read the information from the file
		 * @return user
		 * @throws IOException
		 */
		public static ArrayList<EmployeeAttendence> filereadAttendence() throws IOException {
			ArrayList<EmployeeAttendence> user=null;
			File f=new File("Data3.txt");
			f.createNewFile();
			try {
				FileInputStream fis = new FileInputStream("Data3.txt");
				ObjectInputStream ois = null;
				user=new ArrayList<EmployeeAttendence>();
				ois = new ObjectInputStream(fis);
				user=(ArrayList) ois.readObject();
				return user;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch(EOFException e) {
				return null;
			}
			catch (IOException e) {
				e.printStackTrace();

			}
		}
		try {
			FileOutputStream fos = new FileOutputStream("Data2.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(username);
			oos.close();
			fos.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
			}}

	
	/**
	 * This class filereadAttendence is used to read the information from the file
	 * 
	 * @return user
	 * @throws IOException
	 */
	public static List<EmployeeAttendence> filereadAttendence() throws IOException {
		List<EmployeeAttendence> user = null;
		File f = new File("Data3.txt");
		f.createNewFile();
		try {
			FileInputStream fis = new FileInputStream("Data3.txt");
			ObjectInputStream ois = null;
			user = new ArrayList<EmployeeAttendence>();
			ois = new ObjectInputStream(fis);
			user = (ArrayList) ois.readObject();
			return user;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (EOFException e) {
			return null;
		} catch (IOException e) {
			//e.printStackTrace();
		}

		return user;
	}

	

	/**
	 * This display method display the present days of the employee
	 * 
	 * @throws IOException
	 */
	public static void display() throws IOException {
		int count = 0;
		ArrayList<EmployeePozo> user1 = (ArrayList<EmployeePozo>) File_Read.fileRead();
		List<EmployeeAttendence> user = filereadAttendence();
		List monthlist = new ArrayList();
		FileInputStream fis = new FileInputStream("Data4.txt");
		@SuppressWarnings("resource")
		ObjectInputStream ois = new ObjectInputStream(fis);
		try {
			monthlist = (List) ois.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}
		/**
		 * This class filewriteAttendence is used to write the information of employee into the file
		 * @param obj
		 * @throws IOException
		 */
		public static void filewriteAttendence(EmployeeAttendence obj) throws IOException {
			  ArrayList<EmployeeAttendence> user=  null; 
			  user=(ArrayList<EmployeeAttendence>) filereadAttendence();
			  if(user!=null) {
				  user.add(obj);
			  }
			  else {
				  user = new 	 ArrayList<EmployeeAttendence>();
				  user.add(obj);
			  }
			 try {
				  FileOutputStream fos = new FileOutputStream("Data3.txt");
		          ObjectOutputStream oos = new ObjectOutputStream(fos);
		          oos.writeObject(user);
		          oos.close();
		          fos.close();
			} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}			 

		}
	
	
	public static void monthlyAttendance_write(String date) throws IOException {

		int count = 0;
		ArrayList<EmployeePozo> user1 = (ArrayList<EmployeePozo>) File_Read.fileRead();
		List<EmployeeAttendence> user = filereadAttendence();

		for (EmployeePozo u : user1) {
			count = 0;
			for (EmployeeAttendence p : user) {
				if (p.getName().equals(u.getName())) {
					count++;
				}
			}

			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Date parse = null;
			try {
				parse = format1.parse("2018-12-26");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<MonthlyDetails> monthList = new ArrayList<MonthlyDetails>();
			Calendar c = Calendar.getInstance();
			c.setTime(parse);
			int month = c.get(Calendar.MONTH);
			MonthlyDetails monthDetils = new MonthlyDetails();
			monthDetils.setMonth(month);
			monthDetils.setNoOfDays(count);
			monthList.add(monthDetils);
			try {
				FileOutputStream fos = new FileOutputStream("Data4.txt");
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(monthList);
				
				//System.out.println("ghjkk");
				oos.close();
				fos.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
	}}

	/**
	 * overriding the toString method to print name,password,confirmPassword and
	 * mobile number
	 */
	// @Override
	// public String toString() {
	// EmployeePozo employee = new EmployeePozo();
	// return "EmployeeLogin [name=" + employee.getName() + ", password=" +
	// employee.getPassword() + ",No.Of present days="+ display.count]";
	// }
	
