package com.bean.ams.readwritefile;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import com.bean.ams.adminlogin.AdminLogin;
import com.bean.ams.util.EmployeePozo;
	/**
	 * This class File_Read is used to read and write the  data from the file
	 * @author Batch-'G'
	 *
	 */
	public class File_Read {
		@SuppressWarnings({ "resource", "unchecked", "rawtypes" })
		/**
		 * This fileRead method is used to read the stored data from file
		 * @return user
		 */
		public static List<EmployeePozo> fileRead() {
			List<EmployeePozo> user=null;
			try {

				FileInputStream fis = new FileInputStream("Data2.txt");
				ObjectInputStream ois = null;
			    user = new ArrayList<EmployeePozo>();
			    ois = new ObjectInputStream(fis);
			    user = (ArrayList) ois.readObject();
			    return user;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch(EOFException e) {
				return null;
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(user);
			return user;
		}
		
		/**
		 * This method writeFile is used to store the Registering user details in the file.
		 * @param userRegistration
		 */
		
		public static void writeFile(EmployeePozo userRegistration) {
			List<EmployeePozo> user=null;
			user=new ArrayList<EmployeePozo>();
			fileRead();
			if(	fileRead()==null) {
				
			}
			else {
				user=fileRead();
			}
			user.add(userRegistration);
			try {

				  FileOutputStream fos = new FileOutputStream("Data2.txt");


		          ObjectOutputStream oos = new ObjectOutputStream(fos);
		          oos.writeObject(user);
		          oos.close();
		          fos.close();	
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		/**
		 * This method adminFileRead1 is used to read the admin login credentials and validate them with user input values.
		 * @param name
		 * @param password
		 * @return 
		 */
		public static boolean adminFileRead1(String name,String password) {
			int flag=0;
			
			try {
				AdminLogin user;

				FileInputStream fis = new FileInputStream("Data1.txt");

				@SuppressWarnings("resource")
				ObjectInputStream ois = new ObjectInputStream(fis);
				user=(AdminLogin)ois.readObject();
				if(name.equals(user.getName())&&(password.equals( user.getPassword()))) {
					System.out.println("login successfully");
					flag=1;
					return true;
				}else {
						System.out.println("Invalid Credentials...please enter correct credentials");
						System.out.println("-------------------------");
				}		
				if(flag==1)
				{
					System.out.println("login successful...");
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
		
		public static boolean adminFileRead(String place) {
			int flag=0;
			
			try {
				AdminLogin user;
				FileInputStream fis = new FileInputStream("Data1.txt");
				@SuppressWarnings("resource")
				ObjectInputStream ois = new ObjectInputStream(fis);
				user=(AdminLogin)ois.readObject();
				System.out.println(user.getPlace());
				if(place.equals(user.getPlace())) {
					System.out.println(user.getPlace());
					flag=1;
					return true;
				}else {
						System.out.println("Invalid place...");
						System.out.println("-------------------------");
				}		
				if(flag==1)
				{
					System.out.println("login successful...");
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
		
		/**
		 * This method FileRead1 is used to read the employee login credentials and validate them with user input values.
		 * @param name
		 * @param password
		 */
		public static void FileRead1(String name,String password) {
			int flag=0;
			try {
				AdminLogin user;

				FileInputStream fis = new FileInputStream("Data2.txt");

				@SuppressWarnings("resource")
				ObjectInputStream ois = new ObjectInputStream(fis);
				user=(AdminLogin)ois.readObject();
				if(name.equals(user.getName())&&(password.equals( user.getPassword()))) {
					System.out.println("login successfully");
					flag=1;
				}				
				if(flag==1)
				{
					System.out.println("login successful...");
				}
			
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	
		
	
