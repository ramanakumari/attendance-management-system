package com.bean.ams.adminlogin;

import java.io.Serializable;
/*
 * 
 */
public class AdminLogin implements Serializable {
	private static final long serialVersionUID = 1L;
	String name;
	String password;
	String place;
	//constructor with zero arguments
	public AdminLogin()
	{
		
	}
	
	public String getPlace() {
		return place;
	}

	public boolean setPlace(String place) {
		this.place = place;
		return false;
	}

	//getter method to get name
	public String getName() {
		return name;
	}
	//setter method to set name
	public void setName(String name) {
		this.name = name;
	}
	//getter method to get password
	public String getPassword() {
		return password;
	}
	//setter method to set name
	public void setPassword(String password) {
		this.password = password;
	}
/*	public void setConfirmName(String confirmName) 
	{
		if(employee.validateName(employee.getPassword(),confirmName))
		{
			this.confirmName = confirmName;
		}
		else
		{
			System.out.println("password and confirm password doesn't matches....plz re-enter.....");
		}
	}
	public void setConfirmPassword(String confirmPassword) 
	{
		if(employee.validatePassword(employee.getPassword(),confirmPassword))
		{
			this.confirmPassword = confirmPassword;
		}
		else
		{
			System.out.println("password and confirm password doesn't matches....plz re-enter.....");
		}
	}*/
	
}
