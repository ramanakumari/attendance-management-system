package employeeManagementWrite;

import java.io.Serializable;
/**
 * 
 * @author IMVIZAG
 *
 */
@SuppressWarnings("serial")
public class EmployeeAttendence implements Serializable {
	private String name;
	private String password;
	private String date;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}
